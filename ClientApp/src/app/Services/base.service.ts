import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BaseService {

  constructor() { }

  handleError(err){
    if(err.error instanceof Error){
      console.log(`Client-said error : ${err.error.message}`)
    }else{
      console.log(`Server-said error : ${err.status} - ${err.message}`)
    }
  }
}
