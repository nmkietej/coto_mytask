import { ResponeResult } from './../../Models/ResponeResult';
import { AuthenService } from './../../Services/authen.service';
import { BaseService } from './../../Services/base.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    public _baseService:BaseService,
    public _authenService:AuthenService,
    public router:Router,
  ) { }


  ngOnInit(): void {
    
  }

  public dataResult = new ResponeResult();
  onSubmit(event){
    console.log(event.value)
    this._authenService.checkLogin(event.value.username,event.value.password).subscribe(data => {
      console.log(data)
      if(data.isOk == true){
        localStorage.setItem('userId',data.repData[0].userId);
        this.router.navigate(['/home']);
      }else{
        window.alert("Đăng nhập thất bại. Vui lòng kiểm tra lại tài khoản!")
      }
      
    })
  }

}
