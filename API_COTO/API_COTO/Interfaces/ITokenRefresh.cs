﻿using API_COTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_COTO.Interfaces
{
    public interface ITokenRefresh
    {
        TokenRefresh GetData(string Key);

        bool Save(TokenRefresh tokenRefresh, bool isNew);
    }
}
